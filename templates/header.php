<head>
   <meta charset="utf-8">
   <title>Biblio Drive</title>
   <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
</head>
<div class="row justify-content-around custom-line">
<div class="p-3 mb-2 bg-secondary text-white">
<div class="d-flex flex-row bd-highlight mb-2">
    <div class="col-3">
        <img class="align-text-top" src="logo.png" alt="link" width="150" height="150" />
        </div>
        <div class="col-3">
        <p class="text-left" class="align-text-top">Bienvenu sur la bibliotèque François Mitterrand. Elle est fermée au public, mais il est possible de reserver et retirer vos libre via notre service Biblio Drive !</p>
        </div>  
    </div>
    <div class="col-3">
        </div>
        <button type="button" class="btn btn-danger btn-sm mb-2">Panier</button>

    <div class="input-group">
   <input type="search" class="form-control rounded" placeholder="Saisir le nom de l'auteur à rechercher" aria-label="Search" aria-describedby="search-addon" />
   <button type="button" class="btn btn-primary">Rechercher</button>
</div>
</div>



          
        
        