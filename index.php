<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <?php include "templates/header.php"; ?>

    <body>
    
            
<div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel"alt="Lights" style="width:40%">
   <div class="carousel-inner">
      <div class="carousel-item active">
         <img src="letourdefranceetlafrancedutour.jpg" class="d-block w-100">
      </div>
      <div class="carousel-item">
         <img src="danslamusette.jpg" class="d-block w-100">
      </div>
   </div>
   <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
   <span class="carousel-control-prev-icon" aria-hidden="true"></span>
   <span class="visually-hidden">Previous</span>
   </button>
   <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
   <span class="carousel-control-next-icon" aria-hidden="true"></span>
   <span class="visually-hidden">Next</span>
   </button>
</div>

<section class="vh-100 gradient-custom" >
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-12 col-md-8 col-lg-6 col-xl-5">
        <div class="card bg-secondary text-white" style="border-radius: 1rem;">
          <div class="card bg-secondary p-5 text-center">

            <div class="mb-md-5 mt-md-4 pb-5">

              <h2 class="fw-bold mb-2 text-uppercase">Se Connnecter</h2>
              <p class="text-white-50 mb-5">Saisissez votre identifiant et votre mot de passe !</p>

              <div class="form-outline form-white mb-4">
                <input type="email" id="typeEmailX" class="form-control form-control-lg" />
                <label class="form-label" for="typeEmailX">Identifiant</label>
              </div>

              <div class="form-outline form-white mb-4">
                <input type="password" id="typePasswordX" class="form-control form-control-lg" />
                <label class="form-label" for="typePasswordX">Mot de passe</label>
              </div>

              <button class="btn btn-outline-light btn-lg px-5" type="submit">Connexion</button>

            

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

 
        
    <?php include "templates/footer.php"; ?>
    </body>
</html>
